#![forbid(unsafe_code)]

extern crate hyper;
extern crate mime;
extern crate serde;
extern crate serde_yaml;

use std::fs;
use std::net::SocketAddr;

use hyper::rt::Future;
use hyper::service::service_fn_ok;
use hyper::{Body, Request, Response, Server};

mod config;
use config::ServerConfig;

#[macro_use]
mod error;
mod document;
mod http_error;
mod file_resolver;
mod transformers;

// TODO: Add logging with slog

fn main() {
  let mut config_file = fs::File::open("ferrous.yaml").expect("Unable to open config file");

  let config: ServerConfig =
    serde_yaml::from_reader(&mut config_file).expect("Unabel to parse config file");

  let listen_addr: SocketAddr = config
    .listen_addr
    .clone()
    .parse()
    .expect("Unable to parse listen address");

  let new_service = move || {
    let config = config.clone();

    service_fn_ok(move |req: Request<Body>| -> Response<Body> {
      file_resolver::resolve_request(&config, &req).unwrap_or_else(std::convert::Into::into)
    })
  };

  let server = Server::bind(&listen_addr)
    .serve(new_service)
    .map_err(|e| eprintln!("Server error: {}", e));

  println!("Running on http://{}", listen_addr);
  hyper::rt::run(server);

}
