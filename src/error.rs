/// This macro allows you to easier make a type to simply propagate errors with
/// ease.
/// 
/// Define an error type like this:
/// ```rust
/// multi_error!(FileDocumentError,
///   "an error occured while read the document from file",
///   IoError: std::io::Error,
///   EncodingError: std::str::Utf8Error,
/// );
/// ```
/// And then with a return type of `Result<_, FileDocumentError>` the `?`
/// operator will automatically convert the errors into your error type for you.
/// 
/// You can put a `pub` before the name to allow access from other modules.
/// 
/// I might make this it's own crate because it seems so useful.
#[macro_export]
macro_rules! multi_error {
  ($v:vis $enum_name:ident, $description:expr, $( $variant_name:ident : $variant_error:ty ),+) => {
    #[derive(Debug)]
    $v enum $enum_name {
      $( $variant_name($variant_error) ),*
    }

    impl std::error::Error for $enum_name {
      fn description(&self) -> &str {
        $description
      }
    }

    impl std::fmt::Display for $enum_name {
      fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use std::error::Error;
        write!(f, "{}: ", self.description()).and(match self {
          $( $enum_name::$variant_name(e) => e.fmt(f) ),*
        })
      }
    }

    $(
      impl From<$variant_error> for $enum_name {
        fn from(error: $variant_error) -> Self {
          $enum_name::$variant_name(error)
        }
      }
    )*
  };
}


#[macro_export]
macro_rules! single_error {
  ($v:vis $error_name:ident, $description:expr) => {
    #[derive(Debug)]
    $v struct $error_name;

    impl std::error::Error for $error_name {
      fn description(&self) -> &str {
        $description
      }
    }

    impl std::fmt::Display for $error_name {
      fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use std::error::Error;
        write!(f, "{}", self.description())
      }
    }
  };
}