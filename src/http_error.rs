//! HTTP Error Representation

use hyper::{Body, Response};

/// An Enum representing HTTP Errors.
/// 
/// These are used to map Errors to actual HTTP Responses
/// 
/// Note: This is RFC2324 Compliant
#[allow(dead_code)]
pub enum HttpError {
	BadRequest,
	Unauthorized,
	Forbidden,
	ImATeapot,
	NotFound,
	InternalServerError,
	ServiceUnavailable,
}

impl Into<Response<Body>> for HttpError {
	/// Convert the HttpError into a tuple of the HTTP Status code and an
	/// appropriate message to go along with it.
	fn into(self) -> Response<Body> {
		let (status_code, message) = match self {
			HttpError::BadRequest => (400, "Bad Request"),
			HttpError::Unauthorized => (401, "Not authorized"),
			HttpError::Forbidden => (403, "Forbidden"),
			HttpError::NotFound => (404, "Not Found"),
			HttpError::ImATeapot => (418, "I'm a teapot"),
			HttpError::InternalServerError => (500, "Internal Server Error"),
			HttpError::ServiceUnavailable => (503, "Service Unavailable"),
		};

		Response::builder()
			.status(status_code)
			.body(Body::from(message))
			.unwrap()
	}
}
