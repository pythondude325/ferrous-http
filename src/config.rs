//! This module is for deserializing the server configuration

use serde::Deserialize;
use transformers::external_transformer;

fn default_listen_addr() -> String {
    "0.0.0.0:80".to_string()
}

fn default_document_root() -> String {
    "./".to_string()
}

fn default_index_files() -> Vec<String> {
    vec!["index.html".to_string()]
}

fn default_extensions() -> Vec<String> {
    vec![]
}

/// This struct represent the server configuration once deserialized.
#[derive(Clone, Deserialize)]
pub struct ServerConfig {
    /// The listen address is the IP address and port the server will listen on.
    #[serde(default = "default_listen_addr")]
    pub listen_addr: String,

    /// The document root is the directory that maps to "/"
    #[serde(default = "default_document_root")]
    pub document_root: String,

    /// The index_files is a list of file names to try when the request path is a
    /// directory. The first available file is used.
    #[serde(default = "default_index_files")]
    pub index_files: Vec<String>,

    #[serde(default = "default_extensions")]
    pub extensions: Vec<String>,

    #[serde(default)]
    pub transformers: Vec<external_transformer::ExternalTransformerConfig>
}
