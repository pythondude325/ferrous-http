//! Module holds code related to the Document struct and model

use hyper::{Body, Response};
use std::fs;
use std::io::Read;
use std::path;

/// Converts a file extension to MIME type using some standard file extensions
fn file_extension_to_mime_type(file_extention: &str) -> mime::Mime {
  match file_extention {
    "html" => mime::TEXT_HTML,
    "css" => mime::TEXT_CSS,
    "js" => mime::TEXT_JAVASCRIPT,
    "txt" => mime::TEXT_PLAIN,
    "csv" => mime::TEXT_CSV,
    "tsv" => mime::TEXT_TAB_SEPARATED_VALUES,
    "xml" => mime::TEXT_XML,
    "bmp" => mime::IMAGE_BMP,
    "gif" => mime::IMAGE_GIF,
    "jpeg" | "jpg" => mime::IMAGE_JPEG,
    "png" => mime::IMAGE_PNG,
    "svg" => mime::IMAGE_SVG,
    "json" => mime::APPLICATION_JSON,
    _ => mime::APPLICATION_OCTET_STREAM,
  }
}

/// The `Document` struct is the internal representation of a file in
/// FerrousHttp.
/// 
/// It has a byte vector to hold the contents of a file. It also
/// stores the associated MIME type of the file so that it is served with the
/// correct type.
pub struct Document {
  content_type: mime::Mime,
  content: Vec<u8>,
}


multi_error!(pub FileDocumentError,
  "an error occured while read the document from file",
  IoError: std::io::Error,
  EncodingError: std::str::Utf8Error,
  MimeTypeError: mime::FromStrError
);


impl Document {
  pub fn new(content_type: mime::Mime, content: Vec<u8>) -> Document {
    Document{ content_type, content }
  }

  /// File path to Document
  /// 
  /// Not only does this read a file into a Document, but it also gets an
  /// appropriate MIME type for the document and if the MIME super-type is text,
  /// it ensures that the document is properly UTF-8 encoded.
  pub fn from_file(file_path: &path::Path) -> Result<Document, FileDocumentError> {
    let mut file = fs::File::open(file_path)?;
    let mut content = Vec::new();
    file.read_to_end(&mut content)?;

    let content_type = file_path
      .extension()
      .and_then(std::ffi::OsStr::to_str)
      .map_or(mime::APPLICATION_OCTET_STREAM, file_extension_to_mime_type);

    // If the content type is text then validate the content as utf-8 encoded
    // and mark it as such in the content-type.
    if content_type.type_() == mime::TEXT {
      std::str::from_utf8(&content)?;

      Ok(Document::new(format!("text/{};charset=utf-8", content_type.subtype()).parse()?, content))
    } else {
      Ok(Document::new(content_type, content))
    }
  }

  /// Get the content
  pub fn content(&self) -> &Vec<u8> {
    &self.content
  }

  /// Get the content type
  pub fn content_type(&self) -> &mime::Mime {
    &self.content_type
  }
}

impl Into<Response<Body>> for Document {
  fn into(self) -> Response<Body> {
    Response::builder()
      .header("content-type", self.content_type().as_ref())
      .body(Body::from(self.content)) // TODO: Use streams here
      .unwrap()
  }
}

