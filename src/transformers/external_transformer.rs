use serde::Deserialize;
use mime::Mime;
use document::Document;
use super::{FileTransformer, TransformerError};

#[derive(Clone, Deserialize)]
pub struct ExternalTransformerConfig {
    pub path: String,
    pub output_type: String,
    pub input_types: Vec<String>
}


multi_error!(pub ExternalTransformerError,
  "an error has occured during external transformation",
  IoError: std::io::Error,
  OtherError: &'static str
);

use std::collections::HashSet;

#[derive(Clone)]
pub struct ExternalTransformer {
  input_types: HashSet<Mime>,
  output_type: Mime,
  path_to_executable: String, // Executable should take stdin in JSON format and output in JSON format
}

impl ExternalTransformer {
  pub fn from_config(config: ExternalTransformerConfig) -> Result<ExternalTransformer, mime::FromStrError> {
    Ok(ExternalTransformer {
      path_to_executable: config.path,
      output_type: config.output_type.parse()?,
      input_types: config.input_types.iter().map(|s| s.parse()).collect::<Result<HashSet<_>, _>>()?
    })
  }
}

impl FileTransformer for ExternalTransformer {
  fn transform(&self, input: &Document) -> Result<Document, TransformerError> {
    use std::io::Write;
    use std::process::{Command, Stdio};

    let mut child = Command::new(&self.path_to_executable)
      .stdin(Stdio::piped())
      .stdout(Stdio::piped())
      .spawn()?;
    
    {
      let child_stdin = child.stdin.as_mut().ok_or("Unable to open child stdin")?; // Q: Why isn't mut needed here?
      child_stdin.write_all(&input.content())?;
    }

    let output = child.wait_with_output()?;
    
    Ok(Document::new(self.output_type.clone(), output.stdout))
  }

  fn will_accept(&self, mime_type: &Mime) -> bool {
    self.input_types.contains(mime_type)
  }
}